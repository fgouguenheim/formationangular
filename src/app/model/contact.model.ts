export interface Contact {

  id?: string;

  nom: string;

  tags: string[];

  numsTel: { [key: string]: string };

  adresses: { [key: string]: string };

}

