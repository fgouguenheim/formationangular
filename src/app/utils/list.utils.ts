export class ListUtils {

  static hasIntersection(list1: any[], list2: any[]): boolean {
    return list1.some(it1 => list2.includes(it1)) || list2.some(it2 => list1.includes(it2));
  }

}
