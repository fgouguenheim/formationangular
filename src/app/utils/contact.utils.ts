import {Contact} from '../model/contact.model';
import {ListUtils} from './list.utils';

export class ContactUtils {

  static hasDuplicates(contact: Contact, otherContacts: Contact[]): boolean {
    return otherContacts.some(it =>
      // Pour être sûr que ce n'est pas le même ;-)
      it.id !== contact.id
      && (
      ListUtils.hasIntersection(Object.values(it.numsTel), Object.values(contact.numsTel))
      || ListUtils.hasIntersection(Object.values(it.adresses), Object.values(contact.adresses)))
    );
  }

}
