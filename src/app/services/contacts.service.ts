import {Injectable} from '@angular/core';
import {Contact} from '../model/contact.model';
import {Observable, of, throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  readonly mockContacts: Contact[] = [
    {
      id: '123',
      nom: 'Philippe Dupont',
      tags: ['Famille', 'Proche'],
      numsTel: {domicile: '0123456789'},
      adresses: {domicile: '1 rue de la Paix, 75001 PARIS'},
    },
    {
      id: '456',
      nom: 'Philippinne Martin',
      tags: ['Famille'],
      numsTel: {domicile: '06987654321'},
      adresses: {domicile: '1 place du Capitole, 31000 TOULOUSE'},
    }
  ];

  constructor() {
  }

  findAll(): Observable<Contact[]> {
    return of(this.mockContacts);
  }

  findOne(id?: string): Observable<Contact> {
    if (!id) {
      throw Error('Il faut passer un id');
    }
    const result = this.mockContacts.find(it => it.id === id);
    return result ? of(result) : throwError("Contact non trouvé");
  }
}
