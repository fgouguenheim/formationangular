import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const appRoutes: Routes = [
  {
    path: 'contacts',
    // Solution simple sans children
    // component: ContactsListComponent

    // Solution avec children
    // children: [
    //   ...contactsRoutes
    // ]

    // Solution avec lazy loading
    loadChildren: () => import('./modules/contacts/contacts.module').then(m => m.ContactsModule)

  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
