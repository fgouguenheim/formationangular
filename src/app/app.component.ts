import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  message!: string;

  ngOnInit(): void {
    this.message = this.sayHello();
  }

  private sayHello(): string {
    return 'Hello World from function';
  }
}
