import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactsListComponent } from './contacts-list/contacts-list.component';
import {ContactsRoutingModule} from './contacts-routing.module';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import {FormsModule} from '@angular/forms';



@NgModule({
    declarations: [ContactsListComponent, ContactDetailComponent],
    exports: [
        ContactsListComponent
    ],
  imports: [
    CommonModule,
    ContactsRoutingModule,
    FormsModule
  ]
})
export class ContactsModule { }
