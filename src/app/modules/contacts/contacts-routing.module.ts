import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContactsListComponent} from './contacts-list/contacts-list.component';
import {ContactDetailComponent} from './contact-detail/contact-detail.component';

export const contactsRoutes: Routes = [
  {
    path: '',
    component: ContactsListComponent,
    children: [
      {
        path: ':id',
        component: ContactDetailComponent,
      },
      {
        path: ':id/edit',
        component: ContactDetailComponent,
        data: {editMode: true}
      }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(contactsRoutes)],
  exports: [RouterModule]
})
export class ContactsRoutingModule {
}
