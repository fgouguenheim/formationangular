import {Component, OnDestroy, OnInit} from '@angular/core';
import {ContactsService} from '../../../services/contacts.service';
import {ActivatedRoute, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {Contact} from '../../../model/contact.model';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.scss']
})
export class ContactDetailComponent implements OnInit, OnDestroy {

  contact$?: Observable<Contact>;
  destroy$ = new Subject<void>();
  editMode = false;

  constructor(
    private contactsService: ContactsService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    // V1 : souscription manuelle
    // this.route.paramMap.pipe(
    //   takeUntil(this.destroy$)
    // ).subscribe(params => {
    //   const id = params.get('id') || undefined;
    //   this.contact$ = this.contactsService.findOne(id);
    // });

    // V2 : sans souscription (mieux)
    this.contact$ = this.route.paramMap.pipe(
      switchMap(params => {
        const id = params.get('id') || undefined;
        return this.contactsService.findOne(id);
      })
    );

    this.route.data.subscribe(data => this.editMode = data.editMode);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  toggleEditMode(ignored: MouseEvent): void {
    if (!this.editMode) {
      this.router.navigate(['edit'], {
        relativeTo: this.route
      });
    } else {
      this.router.navigate(['..'], {
        relativeTo: this.route
      });
    }
  }
}
