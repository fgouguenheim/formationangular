import {Component, OnInit} from '@angular/core';
import {ContactsService} from '../../../services/contacts.service';
import {catchError, finalize, tap} from 'rxjs/operators';
import {Contact} from '../../../model/contact.model';
import {Observable, of, throwError} from 'rxjs';

@Component({
  selector: 'app-contacts-list',
  templateUrl: './contacts-list.component.html',
  styleUrls: ['./contacts-list.component.scss']
})
export class ContactsListComponent implements OnInit {

  contacts$?: Observable<Contact[]>;

  constructor(
    private contactsService: ContactsService,
  ) {
  }

  ngOnInit(): void {

    // EXEMPLE DE TRAITEMENT DES ERREURS ET DES COMPLETE SUR LES OBSERVABLES
    const subscription = of(1, 2, 3).pipe(
      // Traitement nouvelle valeur dans le pipe
      tap(value => {
        console.log(`nouvelle valeur : ${value}`);
      }),
      // Traitement erreur dans le pipe
      catchError(err => {
        console.log('erreur');
        const jePeuxTraiterLErreur = this.randomBoolean();
        if (jePeuxTraiterLErreur) {
          return of([]);
        } else {
          return throwError(err);
        }
      }),
      // Traitement complete dans le pipe
      finalize(() => console.log('success'))

      // Souscription : obligatoire pour déclencher le pipe et être notifié des nouvelles valeurs, erreurs et complete
    ).subscribe(
      // Méthodes optionnelles dans la souscription (3 possibles)

      // Traitement nouvelle valeur dans la souscription
      (value) => console.log(`nouvelle valeur bis : ${value}`),
      // Traitement erreur dans la souscription
      () => console.log('erreur bis'),
      // Traitement complete dans la souscription
      () => console.log('success bis'),
    );
    subscription.unsubscribe();

    this.contacts$ = this.contactsService.findAll().pipe(
      tap(contacts => console.log(`Contacts : ${contacts.map(it => it.nom)}`))
    );
  }

  private randomBoolean(): boolean {
    return Boolean(Math.round(Math.random()));
  }
}
